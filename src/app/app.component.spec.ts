import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { Tennis } from './Tennis';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'TennisKata'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('TennisKata');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain('TennisKata app is running!');
  });

  it('should return Love all', () => {
    let game: Tennis = new Tennis("Everyman", "John Doe");
    const score: string = game.getScore();
    expect(score).toBe('Love all');
  });

  it('player one should should win first ball', () => {
    let game: Tennis = new Tennis("Everyman", "John Doe");
    game.playerOneScores();
    const score: string = game.getScore();
    expect(score).toBe('Fifteen,Love');
  });

  it('should return Fifteen all', () => {
    let game: Tennis = new Tennis("Everyman", "John Doe");
    game.playerOneScores();
    game.playerTwoScores();
    const score: string = game.getScore();
    expect(score).toBe('Fifteen all');
  });

  it('player 2 should win first two balls', () => {
    let game: Tennis = new Tennis("Everyman", "John Doe");
    game.playerTwoScores();
    game.playerTwoScores();
    const score: string = game.getScore();
    expect(score).toBe('Love,Thirty');
  });

  it('player 1 should win first three balls', () => {
    let game: Tennis = new Tennis("Everyman", "John Doe");
    game.playerOneScores();
    game.playerOneScores();
    game.playerOneScores();
    const score: string = game.getScore();
    expect(score).toBe('Forty,Love');
  });

  it('should return Deuce', () => {
    let game: Tennis = new Tennis("Everyman", "John Doe");
    game.playerOneScores();
    game.playerOneScores();
    game.playerOneScores();

    game.playerTwoScores();
    game.playerTwoScores();
    game.playerTwoScores();
    const score: string = game.getScore();
    expect(score).toBe('Deuce');
  });

  it('player 1 wins game', () => {
    let game: Tennis = new Tennis("Everyman", "John Doe");
    game.playerOneScores();
    game.playerOneScores();
    game.playerOneScores();
    game.playerOneScores();
    const score: string = game.getScore();
    expect(score).toBe('Everyman wins');
  });

  it('player 2 wins game', () => {
    let game: Tennis = new Tennis("Everyman", "John Doe");
    game.playerOneScores();
    game.playerOneScores();

    game.playerTwoScores();
    game.playerTwoScores();
    game.playerTwoScores();
    game.playerTwoScores();
    const score: string = game.getScore();
    expect(score).toBe('John Doe wins');
  });

  it('player 1 advantage', () => {
    let game: Tennis = new Tennis("Everyman", "John Doe");
    game.playerOneScores();
    game.playerOneScores();
    game.playerOneScores();
    game.playerOneScores();
    game.playerOneScores();

    game.playerTwoScores();
    game.playerTwoScores();
    game.playerTwoScores();
    game.playerTwoScores();
    const score: string = game.getScore();
    expect(score).toBe('Advantage Everyman');
  });
});
