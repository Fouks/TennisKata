export class Tennis {
  private playerOneScore: number = 0;
  private playerTwoScore: number = 0;
  private playerTwoName: string;
  private playerOneName: string;

  constructor(playerOneName: string, playerTwoName: string) {
    this.playerOneName = playerOneName;
    this.playerTwoName = playerTwoName;
  }

  private isDeuce(): boolean {
    return (
      this.playerOneScore >= 3 && this.playerTwoScore == this.playerOneScore
    );
  }

  private playerWithHighestScore(): string {
    return this.playerOneScore > this.playerTwoScore
      ? this.playerOneName
      : this.playerTwoName;
  }

  private hasWinner(): boolean {
    return this.playerTwoScore >= 4 &&
      this.playerTwoScore >= this.playerOneScore + 2
      ? true
      : this.playerOneScore >= 4 &&
        this.playerOneScore >= this.playerTwoScore + 2
      ? true
      : false;
  }

  private hasAdvantage(): boolean {
    return this.playerTwoScore >= 4 &&
      this.playerTwoScore == this.playerOneScore + 1
      ? true
      : this.playerOneScore >= 4 &&
        this.playerOneScore == this.playerTwoScore + 1
      ? true
      : false;
  }

  public playerOneScores(): void {
    this.playerOneScore++;
  }

  public playerTwoScores(): void {
    this.playerTwoScore++;
  }

  private translateScore(score: number) {
    switch (score) {
      case 3:
        return 'Forty';
      case 2:
        return 'Thirty';
      case 1:
        return 'Fifteen';
      case 0:
        return 'Love';
    }
    throw new Error('Illegal score: ' + score);
  }

  getScore() {
    return this.hasWinner()
      ? this.playerWithHighestScore() + ' wins'
      : this.hasAdvantage()
      ? 'Advantage ' + this.playerWithHighestScore()
      : this.isDeuce()
      ? 'Deuce'
      : this.playerOneScore == this.playerTwoScore
      ? this.translateScore(this.playerOneScore) + ' all'
      : this.translateScore(this.playerOneScore) +
        ',' +
        this.translateScore(this.playerTwoScore);
  }
}
